<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center" style="font-family: verdana, arial, helvetica; font-size: small;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" style="font-family: verdana, arial, helvetica; font-size: small;">
        
        <tr valign="top">
          <td>
              <div style="padding-left: 1em;">Dispatch to: <br />
                  <span style="font-size: large;">
                      <?php echo uc_order_address($order,'delivery'); ?><br />
                      
                  </span><br />
                  <hr><br />
                   </div>
                </td>
               </tr>
            </table>
          </td>
        </tr>
</table>

<table valign="top" >
          <td height="50" valign="top">
              <b>Order ID: <?php echo $order->order_id?></b><br/>
                <?php echo t('Thank you for buying from Imagine Wholesale Clearance Marketplace.'); ?><br/>
          </td> 
</table>

            <table frame="box" width="100%" cellspacing="0" cellpadding="10" style="font-family: verdana, arial, helvetica; font-size: small;">
                    <tr>
                      <td valign="top" width="50%">
                        <b><?php echo t('Delivery address:'); ?></b><br />
                        <?php echo $order->delivery_address; ?><br />
                        <br />
                      </td>
                      <td valign="top" width="50%">
                          <table width="100%" cellspacing="0" cellpadding="10" style="font-family: verdana, arial, helvetica; font-size: small;">
                              <tr>
                                  <td valign="top" width="35%">
                                      <b>Order Date: </b>
                                   </td>     
                                   <td valign="top" width="65%">
                                       <?php echo format_date($order->created, 'custom',  'j M Y'); ?>
                                   </td>
                              </tr>
                              <tr>
                                  <td valign="top" width="35%">
                                      <b>Delivery Service: </b>	
                                  </td>
                                  <td valign="top" width="65%">Standart </td>
                              </tr>
                              <tr>
                                  <td valign="top" width="35%">
                                      <b>Buyer Name: </b>
                                   </td>     
                                   <td valign="top" width="65%">
                                       <?php echo $order->delivery_first_name.' '.$order->delivery_last_name ; ?>
                                   </td>
                              </tr>
                              
                          </table>
                        </td>
                    </tr>
                  </table>

        <table border="1" width="100%" cellspacing="0" cellpadding="10" style="font-family: verdana, arial, helvetica; font-size: small;">
                       <tr align="center">
                           <th >Quantity</th>
                          <th >Product Details</th>
                           <th>Items In Joblot</th>
                           <th>Total Weight (KG)</th>
                       </tr>
                      
                           <?php
                           if (is_array($order->products)) {
                                foreach ($order->products as $product_in_order) {
                                    if($product_in_order->uid == $uid){
                                     ?>
                       <tr>
                       <td>
                            <?php echo $product_in_order->qty; ?>  
                           </td>
                             <td valign="top" nowrap="nowrap">
                              
                              <b><?php echo $product_in_order->title; ?></b><br/>
                              <?php echo t('SKU: ') . $product_in_order->model; ?><br/>
                              <?php echo t('EAN: ') . $product_in_order->ean; ?><br /><br />
                            </td>
                            <td width="50%">
                              <?php echo $product_in_order->it_in_joblot ; ?>
                            </td>
                               <td width="50%">
                              <?php echo $product_in_order->weight ; ?>
                            </td>
                            
                       </tr>
                                    <?php   }
                                    
                                    }
                                }
                           ?>
                   <tr>
                           <td colspan="4" align="justify">
                             <?php echo t('Comments: ') . $order->message; ?>  
                             
                           </td>
                               
                       </tr>       
                       
              </table>
<span class="print-line"><i class="fa fa-print fa-2x"></i><div class="print-btn btn btn-info">     
<?php print  l('Printable Packing slip', 'seller/my_orders/'. $order->order_id.'/'.$uid.'/invoice/print', array('html' => TRUE, 'attributes' => array('target' => '_blank'))); ?>
    </div></span>
<div > <?php 
if (!array_search('seller', $variables['user']->roles)) {print drupal_get_form('seller_reports_mail_invoice_form',$order->order_id,$uid); }?></div>
             
