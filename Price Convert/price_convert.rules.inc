<?php
/**
 * @file
 * Commerce Multicurrency rules integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function price_convert_rules_action_info() {
$actions = array();
  
$actions['commerce_multicurrency_calculate_priceUSD_to_UAH'] = array(
    'label' => t('Calculate price USD to UAH'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product' ,
        'label' => t('Commerce product'),
      ),
        ),
    'group' => t('Commerce Multicurrency')
  );
  
  $actions['commerce_multicurrency_calculate_priceEUR_to_UAH'] = array(
    'label' => t('Calculate price EUR to UAH'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product' ,
        'label' => t('Commerce product'),
      ),
        ),
    'group' => t('Commerce Multicurrency')
  );
      
  return $actions;
}
