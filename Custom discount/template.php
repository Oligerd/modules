<?php

/**
 * @file
 * template.php
 */
function ledart_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
 
  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu so it
    // does not affect the navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    //Here we need to change from ==1 to >=1 to allow for multilevel submenus
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] >= 1)) {
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
      // Generate as standard dropdown.
      //$element['#title'] .= ' <span class="caret"></span>'; Smartmenus plugin add's caret
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;
 
      // Set dropdown trigger element to # to prevent inadvertant page loading
      // when a submenu link is clicked.
      $element['#localized_options']['attributes']['data-target'] = '#';
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      //comment element bellow if you want your parent menu links to be "clickable"
      //$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
    }
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function ledart_preprocess_rules_link(&$vars) {
    if ($vars['rules_link']->name == 'link_for_webform'){
         $vars['classes_array'][] = 'btn btn-success btn-block';
//        $vars['attr'] = drupal_attributes($attributes = array(
////                    'title' => $vars['title'], 
//                    'class' => array(
//                        '1' => $vars['classes_array']),
//                    'rel' => 'nofollow',
//                ));
    }
}


function ledart_commerce_price_formatted_components($variables) {
     // Add the CSS styling to the table.
  drupal_add_css(drupal_get_path('module', 'commerce_price') . '/theme/commerce_price.theme.css');
  // Build table rows out of the components.
  $rows = array();
  foreach ($variables['components'] as $name => $component) {
      switch ($name ) {
        case 'base_price':
            $rows[] = array(
              'data' => array(
        //        array(
        //          'data' => t('Base price'), 
        //          'class' => array('component-title'),
        //        ),
                array(
                  'data' => $component['formatted_price'], 
                  'class' => array('component-price-base'),
                ),
              ), 

        //      'class' => array(drupal_html_class('component-type-' . $name)),
            );
            break;

        case 'discount':
            $rows[] = array(
              'data' => array(
        //        array(
        //          'data' => t('Discount'), 
        //          'class' => array('component-title'),
        //        ),
                array(
                  'data' => $component['formatted_price'], 
                  'class' => array('component-price-discount'),
                ),
              ), 

        //      'class' => array(drupal_html_class('component-type-' . $name)),
            );
            break;
           case 'commerce_price_formatted_amount':
            $rows[] = array(
              'data' => array(
        //        array(
        //          'data' => t('Sell price'), 
        //          'class' => array('component-title'),
        //        ),
                array(
                  'data' => $component['formatted_price'], 
                  'class' => array('component-total'),
                ),
              ), 

        //      'class' => array(drupal_html_class('component-type-' . $name)),
            );
             break;  
            default:
                $rows[] = array(
              'data' => array(
                array(
                  'data' => $component['title'], 
                  'class' => array('component-title'),
                ),
                array(
                  'data' => $component['formatted_price'], 
                  'class' => array('component-total'),
                ),
              ), 
              'class' => array(drupal_html_class('component-type-' . $name)),
            );
              }
          }
  return theme('table', array('rows' => $rows, 'attributes' => array('class' => array('commerce-price-formatted-components'))));
}

