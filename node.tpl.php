<?php

//кнопки социальных сетей
<?php
    
$urlvk = 'http://vk.com/share.php?url=' . 'http://' . $_SERVER["HTTP_HOST"] . $node_url;
$urlfb = 'http://www.facebook.com/sharer.php?u=' . 'http://' . $_SERVER["HTTP_HOST"] .$node_url;
$urltw = 'https://twitter.com/share?text=' . urlencode($title) . '&url=http://' . $_SERVER["HTTP_HOST"] . $node_url;
$urlgoo = 'https://plus.google.com/share?url=' . 'http://' . $_SERVER["HTTP_HOST"] . $node_url .'/';

if (isset($node ->field_picture)){
$brm= file_create_url($node ->field_picture['und'][0]['uri']);
$urlpin = 'https://www.pinterest.com/pin/create/button/?url='. 'http://' . $_SERVER["HTTP_HOST"] . $node_url .'&media='.$brm.'&description=' .urlencode($title);
}else
{$urlpin = 'https://www.pinterest.com/pin/create/button/?url='. 'http://' . $_SERVER["HTTP_HOST"] . $node_url .'&description=' .urlencode($title);
}
 ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['disqus']);
      print render($content);
    ?>
  </div>
    <div class="block clearfix">   
<a href="<?=$urlfb?>" class="soc-icon facebook"><span class="fa fa-facebook"></span></a>
<a href="<?=$urltw?>" class="soc-icon twitter"><span class="fa fa-twitter"></span></a>
<a href="<?=$urlgoo?>" class="soc-icon google-plus"><span class="fa fa-google-plus"></span></a>
<a href="<?=$urlpin?>" class="soc-icon pinterest"><span class="fa fa-pinterest-p"></span></a>
</div>
<?php print render($content['links']); 
            print render($content['disqus']);
            print render($content['comments']); ?>

</div>
